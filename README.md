# Data Processing Pipeline for AI Projects

Welcome to our AI project's repository! This README provides a detailed guide on our data processing pipeline, ensuring clarity and reproducibility in our workflows. Here, you will find step-by-step instructions on how we handle data cleaning, feature extraction, feature engineering, data splitting, versioning, modeling, and MLOps - Neptune.ai.

## Table of Contents
- [Project Overview](#project-overview)
- [Data Processing Pipeline](#data-processing-pipeline)
  - [Data Cleaning](#data-cleaning)
  - [Feature Extraction](#feature-extraction)
  - [Feature Engineering](#feature-engineering)
  - [Data Splitting](#data-splitting)
  - [Data Versioning](#data-versioning)
  - [Modeling](#modeling)
  - [MLOps - Neptune.ai](#mlops---neptuneai)
- [Getting Started](#getting-started)
- [Contributing](#contributing)
- [License](#license)

## Project Overview
Briefly describe the goals and objectives of your AI project. Include any relevant context or links to resources that provide additional information.

## Data Processing Pipeline

### Data Cleaning & Feature Extraction & Feature Engineering & Data Splitting
**Objective:** Ensure that the data is free of errors or inconsistencies, and is ready for analysis and modeling.

**Steps:**
1. **Stemming**
2. **TF-IDF**

**Tools & Libraries:**
- Pandas for data manipulation
- NumPy for numerical operations
- Scikit-learn for preprocessing


### Data Versioning
**Objective:** Keep track of different versions of datasets used in the project.

**Steps:**
1. **Version Control:** Use tools like DVC to manage and version control datasets.
2. **Documentation:** Document the changes in each version, including the rationale for changes and impact on models.

**Tools & Libraries:**
- GitLab

### Modeling
**Objective:** Create a machine learning model. Model used: Logistic Regression, Random Forest, SVC

**Tools & Libraries:**
- Scikit-learn

### MLOps - Neptune.ai
**Objective:** Neptune is a metadata store for MLOps, built for research and production teams that run a lot of experiments. It gives you a central place to log, store, display, organize, compare, and query all metadata generated during the machine learning lifecycle.

**Tools & Libraries:**
- Neptune.ai

## Getting Started
Provide instructions on how to set up and run the project locally. This section should include:
- Installation of required libraries
- How to run the scripts
- Example commands

```bash
# Clone the repository
git clone https://github.com/your-username/your-project-name.git
cd your-project-name

# Install dependencies
pip install -r requirements.txt

# Run the data processing script
python data_processing.py

# Run the data train script
python train.py
```

## Experimented on Neptune.ai
Contohnya ketika menggunakan <b>Logistic Regression</b>
<div align="center">
  <img src="assets/img/SEN2.png" alt="Sen2" width="1920" height="1080">
</div>


## Contributing
Encourage other developers to contribute to your project by providing guidelines on how they can contribute.

## License
Specify the license under which your project is made available. This informs users of what they can and cannot do with your code.
